package ru.rsreu.oop_lab1.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import ru.rsreu.oop_lab1.task1.Task1;
import ru.rsreu.oop_lab1.task10.Task10;
import ru.rsreu.oop_lab1.task12.Task12;
import ru.rsreu.oop_lab1.task13.Task13;
import ru.rsreu.oop_lab1.task14.Task14;
import ru.rsreu.oop_lab1.task2.Task2;
import ru.rsreu.oop_lab1.task3.Task3;
import ru.rsreu.oop_lab1.task4.Task4;
import ru.rsreu.oop_lab1.task5.Task5;
import ru.rsreu.oop_lab1.task6.Task6;
import ru.rsreu.oop_lab1.task7.Task7;
import ru.rsreu.oop_lab1.task8.Task8;
import ru.rsreu.oop_lab1.task9.Task9;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    @FXML
    private TextArea task1_textArea, task6_textarea_del, task7_textareaHara, task9_textarea, task13_textarea,
            task3_textarea, task8_textarea;

    @FXML
    private TextField task2_field_shesn, task2_field_dec, task3_field_byte, task3_field_long, task3_field_double,
            task3_field_int, task4_filed_symbol, task4_filed_celoe, task6_field_celoe, task7_fieldString,
            task10_fieldA, task10_fieldB, task10_fieldC, task12_field_patch, task13_field_path, task14_field_path,
            task8_field_string;

    @FXML
    private Label task2_errlabel, task4_label_err, task5_label_info, task6_label_err, task9_label_info;

    private Task1 task1;
    private Task2 task2;
    private Task3 task3;
    private Task4 task4;
    private Task5 task5;
    private Task6 task6;
    private Task7 task7;
    private Task8 task8;
    private Task9 task9;
    private Task10 task10;
    private Task12 task12;
    private Task13 task13;
    private Task14 task14;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        task1 = new Task1();
        task2 = new Task2();
        task3 = new Task3();
        task4 = new Task4();
        task5 = new Task5(task4);
        task6 = new Task6();
        task7 = new Task7();
        task8 = new Task8();
        task9 = new Task9();
        task12 = new Task12();
        task13 = new Task13();
        task14 = new Task14();
    }

    public void setTask1_but_run(ActionEvent event) {
        task1.run();
    }

    public void setTask1_but_otobrazit(ActionEvent event) {
        task1_textArea.setText(task1.print());
    }

    public void setTask1_but_clean(ActionEvent event) {
        task1_textArea.clear();
    }

    public void setTask2_but_preo(ActionEvent event) {
        task2.setShes(task2_field_shesn.getText());
        if (task2.proverka() == false) {
            task2_errlabel.setText("Ошибка: введите корректное значение");
        } else {
            task2.perevod();
            task2_field_dec.setText(task2.getDec());
            task2_errlabel.setText("");
        }
    }

    public void setTask3_but_save(ActionEvent event) {
        task3.setaByte(Byte.parseByte(task3_field_byte.getText()));
        task3.setaDouble(Double.parseDouble(task3_field_double.getText()));
        task3.setaLong(Long.parseLong(task3_field_long.getText()));
        task3.setAnInt(Integer.parseInt(task3_field_int.getText()));

        task3_textarea.appendText("Class: " + task3.getClass().getName());
        task3_textarea.appendText("\nMetod: ");
        for (Method method : task3.getClass().getMethods()) {
            task3_textarea.appendText(method.getName() + ", ");
        }
        task3_textarea.appendText("\nField: ");
        for (Field field : task3.getClass().getDeclaredFields()) {
            task3_textarea.appendText(field.getName() + "(" + field.getType() + "), " + field.toString());
        }
    }

    public void setTask4_but_preo(ActionEvent event) {
        task4.setCeloe(Integer.parseInt(task4_filed_celoe.getText()));
        if (task4.check()) {
            task4.preo();
            task4_filed_symbol.setText(String.valueOf(task4.getSymbol()));
            task5_label_info.setText(task5.printInfo());
            task4_label_err.setText("");
        } else {
            task4_label_err.setText("Symbol not fount");
        }
    }

    public void setTask6_but_search(ActionEvent event) {
        task6.setNumber(Integer.parseInt(task6_field_celoe.getText()));
        task6_label_err.setText("");
        if (task6.check()) {
            task6.search();
            task6_textarea_del.setText(task6.getStringBuffer().toString());
        } else {
            task6_label_err.setText("Неккоректное значение");
        }
    }

    public void setTask7_butString(ActionEvent event) {
        task7.setText(task7_fieldString.getText());
        task7_textareaHara.setText(task7.getInfo());
    }

    public void setTask8_butString(ActionEvent event) {
        task8.setText(task8_field_string.getText());
        task8_textarea.setText(task8.getInfo());
    }

    public void setTask9_butSubmit(ActionEvent event) {
        task9.setText(task9_textarea.getText());
        if (task9.check()) {
            task9_label_info.setText("Правильная строка");
        } else {
            task9_label_info.setText("Неправильная строка");
            task9_textarea.selectRange(task9.position(), task9.position() + 1);
        }
    }

    public void setTask10_but_sub(ActionEvent event) {
        double a = Double.parseDouble(task10_fieldA.getText());
        double b = Double.parseDouble(task10_fieldB.getText());
        double c = Double.parseDouble(task10_fieldC.getText());
        task10 = new Task10(a, b, c);

        StringBuffer rezult = new StringBuffer();
        switch (task10.countx()) {
            case 2:
                rezult.append("x1 = " + task10.x1() + "\nx2 = " + task10.x2());
                break;
            case 0:
                rezult.append("Корней нет");
                break;
            case 1:
                rezult.append("x1 = " + task10.x1());
                break;
        }

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText(rezult.toString());
        alert.showAndWait();
    }

    public void setTask12_but_run() {
        task12.setPatch(task12_field_patch.getText());
        task12.read();
        task12.search();

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText("Количество случаев: " + task12.count());
        alert.showAndWait();
    }

    public void setTask13_but_open() {
        task13_textarea.appendText("Чтение файла\n");
        task13.setPatch(task13_field_path.getText());
        task13.read();
        task13_textarea.appendText("Чтение файла завершено\n");
    }

    public void setTask13_but_check() {
        task13_textarea.appendText(task13.print());
    }

    public void setTask13_but_op() {
        task13_textarea.appendText("Формирвоание массива чисел\n");
        task13.search();
        task13_textarea.appendText("Начало записи в файл\n");
        task13.write();
        task13_textarea.appendText("Файл записан\n");
    }

    public void setTask14_but_run() {
        task14.setPath(task14_field_path.getText());
        task14.read();
        task14.search();

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText("Максимальный элемент из 5: " + task14.max());
        alert.showAndWait();
    }

}
