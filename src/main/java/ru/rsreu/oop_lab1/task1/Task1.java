package ru.rsreu.oop_lab1.task1;

public class Task1 {

    private StringBuffer text;

    public Task1() {
        text = new StringBuffer();
    }

    public String print() {
        return text.toString();
    }

    public void run() {
        for (int i = 32; i < 255; i++) {
            text.append((char) i);
        }
    }

    public void clean() {
        text.setLength(0);
    }

}
