package ru.rsreu.oop_lab1.task10;

public class Task10 {

    double a, b, c;

    public Task10(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int countx() {
        double d = disc();
        if (d < 0) {
            return 0;
        } else if (d == 0) {
            return 0;
        } else {
            return 2;
        }
    }

    public double x1() {
        return (-b + Math.sqrt(disc())) / (2 * a);
    }

    public double x2() {
        return (-b - Math.sqrt(disc())) / (2 * a);
    }

    private double disc() {
        return b * b - 4 * a * c;
    }
}
