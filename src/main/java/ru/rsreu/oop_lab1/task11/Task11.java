package ru.rsreu.oop_lab1.task11;

import java.util.Scanner;

public class Task11 implements Runnable {

    private double a, b, sum;

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void run() {
        input();
        sum();
        print();
    }

    private void input() {
        Scanner in = new Scanner(System.in);
        System.out.print("Vvedite a: ");
        setA(in.nextDouble());
        System.out.print("Vvedite b: ");
        setB(in.nextDouble());
    }

    private void print() {
        System.out.println("\nSumma: " + sum);
    }

    public void sum() {
        sum = a + b;
    }

}
