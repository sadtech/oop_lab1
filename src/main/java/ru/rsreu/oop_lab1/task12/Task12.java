package ru.rsreu.oop_lab1.task12;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task12 {

    private String patch;
    private String textFile;
    private ArrayList<Integer> arrayList;

    public Task12() {
        arrayList = new ArrayList<>();
    }

    public void setPatch(String patch) {
        this.patch = patch;
    }

    public void read() {
        try (FileReader reader = new FileReader(patch)) {
            char[] buf = new char[256];
            textFile = "";
            int c;
            while ((c = reader.read(buf)) > 0) {
                if (c < 256) {
                    buf = Arrays.copyOf(buf, c);
                }
                textFile += String.copyValueOf(buf);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void search() {
        Pattern p = Pattern.compile("[\\d-]+");
        Matcher m = p.matcher(textFile);
        while (m.find()) {
            arrayList.add(Integer.parseInt(m.group()));
        }
    }

    public int count() {
        int count = 0;
        for (int i = 0; i < arrayList.size() - 1; i = i + 2) {
            if (arrayList.get(i) > 0 && arrayList.get(i + 1) > 0) {
                count++;
            }
        }
        return count;
    }

}
