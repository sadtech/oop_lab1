package ru.rsreu.oop_lab1.task13;

public class Complex {

    private double re;
    private double im;

    public Complex(double im, double re) {
        this.re = re;
        this.im = im;
    }

    public double getRe() {
        return re;
    }

    public double getIm() {
        return im;
    }

}
