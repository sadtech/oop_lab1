package ru.rsreu.oop_lab1.task13;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task13 {

    private String patch;
    private String textFile;
    private ArrayList<Complex> arrayList;

    public Task13() {
        arrayList = new ArrayList<>();
    }

    public void setPatch(String patch) {
        this.patch = patch;
    }

    public void read() {
        try (FileReader reader = new FileReader(patch)) {
            char[] buf = new char[256];
            textFile = "";
            int c;
            while ((c = reader.read(buf)) > 0) {
                if (c < 256) {
                    buf = Arrays.copyOf(buf, c);
                }
                textFile += String.copyValueOf(buf);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void search() {
        Pattern p = Pattern.compile("[\\d-.,]+");
        Matcher m = p.matcher(textFile);
        ArrayList<Double> arrayList = new ArrayList<>();
        while (m.find()) {
            arrayList.add(Double.parseDouble(m.group().replace(',', '.')));
        }
        for (int i = 0; i < arrayList.size() - 1; i = i + 2) {
            this.arrayList.add(new Complex(arrayList.get(i), arrayList.get(i + 1)));
        }
    }

    public void write() {
        try (FileWriter writer = new FileWriter("/Users/upagge/IdeaProjects/Rsreu/oop_lab1/src/main/resources/text/task13_out.txt", false)) {
            writer.write(print());
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public String print() {
        StringBuffer stringBuffer = new StringBuffer();
        for (Complex elem : arrayList) {
            stringBuffer.append("Im: " + elem.getIm() + " Re: " + elem.getRe() + ";\n");
        }
        return stringBuffer.toString();
    }

}
