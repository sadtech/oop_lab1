package ru.rsreu.oop_lab1.task14;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task14 {

    private String path;
    private ArrayList<Integer> arrayList;
    private String textFile;

    public Task14() {
        arrayList = new ArrayList<>();
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void read() {
        try (FileReader reader = new FileReader(path)) {
            char[] buf = new char[256];
            textFile = "";
            int c;
            while ((c = reader.read(buf)) > 0) {
                if (c < 256) {
                    buf = Arrays.copyOf(buf, c);
                }
                textFile += String.copyValueOf(buf);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void search() {
        Pattern p = Pattern.compile("[\\d-]+");
        Matcher m = p.matcher(textFile);
        while (m.find()) {
            arrayList.add(Integer.parseInt(m.group()));
        }
    }

    public int max() {
        int max = arrayList.get(0);
        for (int i = 1; i < 5; i++) {
            if (arrayList.get(i) > max) {
                max = arrayList.get(i);
            }
        }
        return max;
    }
}
