package ru.rsreu.oop_lab1.task2;

public class Task2 {

    private String shes;
    private Integer dec;

    public void setShes(String shes) {
        this.shes = shes;
    }

    public void perevod() {
        this.dec = Integer.parseInt(shes.substring(1), 16);
    }

    public String getDec() {
        return dec.toString();
    }

    public boolean proverka() {
        if (shes.matches("[$]+[A-Fa-f0-9]+")) {
            return true;
        } else {
            return false;
        }
    }
}
