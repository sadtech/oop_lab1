package ru.rsreu.oop_lab1.task3;

public class Task3 {

    private Byte aByte;
    private Long aLong;
    private Integer anInt;
    private Double aDouble;

    public void setaByte(byte aByte) {
        this.aByte = aByte;
    }

    public void setaLong(long aLong) {
        this.aLong = aLong;
    }

    public void setAnInt(int anInt) {
        this.anInt = anInt;
    }

    public void setaDouble(double aDouble) {
        this.aDouble = aDouble;
    }
}
