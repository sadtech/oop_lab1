package ru.rsreu.oop_lab1.task4;

public class Task4 {

    private int celoe;
    private char symbol;

    public char getSymbol() {
        return symbol;
    }

    public int getCeloe() {
        return celoe;
    }

    public void setCeloe(int celoe) {
        this.celoe = celoe;
    }

    public void preo() {
        symbol = (char) celoe;
    }

    public boolean check() {
        if (celoe > 31 && celoe < 255) {
            return true;
        } else {
            return false;
        }
    }
}
