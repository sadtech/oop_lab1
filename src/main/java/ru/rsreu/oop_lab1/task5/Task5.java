package ru.rsreu.oop_lab1.task5;

import ru.rsreu.oop_lab1.task4.Task4;

public class Task5 {

    private Task4 task4;

    public Task5(Task4 task4) {
        this.task4 = task4;
    }

    public String printInfo() {
        int temp = task4.getCeloe();
        if ((temp >= 32 && temp <= 47) || (temp >= 58 && temp <= 64) || (temp >= 91 && temp <= 96)) {
            return "Символ";
        } else if (temp >= 48 && temp <= 57) {
            return "Цифра";
        } else if (temp >= 65 && temp <= 90) {
            return "Печатная буква";
        } else if (temp >= 97 && temp <= 122) {
            return "Прописная буква";
        } else {
            return "Другое";
        }
    }
}
