package ru.rsreu.oop_lab1.task6;

public class Task6 {

    private int number;
    private StringBuffer stringBuffer;

    public Task6() {
        this.stringBuffer = new StringBuffer();
    }

    public void setNumber(int number) {
        this.number = number;
        this.stringBuffer.setLength(0);
    }

    public boolean check() {
        if (number > 3) {
            return true;
        } else {
            return false;
        }
    }

    public void search() {
        for (int i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                stringBuffer.append(String.valueOf(i) + "\n");
            }
        }
    }

    public StringBuffer getStringBuffer() {
        return stringBuffer;
    }
}
