package ru.rsreu.oop_lab1.task7;

public class Task7 {

    private String text;

    public void setText(String text) {
        this.text = text;
    }

    public String getInfo() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Длинна строки: " + text.length() + "\nПервый символ строки: " + text.charAt(0) + "\nHashCode: " + text.hashCode());
        return stringBuffer.toString();
    }
}
