package ru.rsreu.oop_lab1.task8;

public class Task8 {

    private StringBuffer text;

    public void setText(String text) {
        this.text = new StringBuffer(text);
    }

    public String getInfo() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Длинна строки: " + text.length() + "\nПервый символ строки: " + text.charAt(0) +
                "\nHashCode: " + text.hashCode() + "\nПоследний символ: " + text.charAt(text.length() - 1));
        return stringBuffer.toString();
    }
}
