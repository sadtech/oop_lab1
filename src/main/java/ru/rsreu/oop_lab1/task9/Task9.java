package ru.rsreu.oop_lab1.task9;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task9 {

    private String text;

    public void setText(String text) {
        this.text = text;
    }

    public boolean check() {
        if (text.matches("[\\w ]+")) {
            return true;
        } else {
            return false;
        }
    }

    public int position() {
        Pattern pattern = Pattern.compile("[^A-Za-z0-9 ]");
        Matcher matcher = pattern.matcher(text);
        int index = 0;
        if (matcher.find()) {
            index = matcher.start();
        }
        return index;
    }
}
